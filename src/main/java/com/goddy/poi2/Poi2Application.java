package com.goddy.poi2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Poi2Application {

    public static void main(String[] args) {
        SpringApplication.run(Poi2Application.class, args);
    }
}
