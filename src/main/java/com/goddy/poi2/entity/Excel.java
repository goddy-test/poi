package com.goddy.poi2.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Slf4j
public class Excel {

    private String title;

    private Map<String, List<String>> dataMap;

    HttpServletResponse response;

    HSSFWorkbook workbook;

    public Excel() {
        dataMap =  new HashMap<>();
        workbook = new HSSFWorkbook();
    }

    public Excel(String title, Map<String, List<String>> dataMap) {
        this.title = title;
        this.dataMap = dataMap;
        workbook = new HSSFWorkbook();
    }

    public void build() throws Exception {

        HSSFSheet sheet = workbook.createSheet(title);

        int rowCounter = 0;
        for (Map.Entry<String, List<String>> entry : dataMap.entrySet()) {
            String title = entry.getKey();
            List<String> data = entry.getValue();

            HSSFRow row = sheet.createRow(rowCounter);
            for (int i = 0; i < data.size(); i ++) {
                HSSFCell cell = row.createCell(i);
                cell.setCellValue(data.get(i));
            }
            rowCounter ++;
        }
    }

    public Excel withBuild() throws Exception {
        build();
        return this;
    }

    public void export(HttpServletResponse response) throws IOException {
        OutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            response.setContentType("application/ms-excel;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename="
                    .concat(String.valueOf(URLEncoder.encode(title + ".xls", String.valueOf(StandardCharsets.UTF_8)))));
            workbook.write(outputStream);
        } catch (IOException e) {
            log.error("Output stream error!");
            e.printStackTrace();
        } finally {
            assert outputStream != null;
            outputStream.close();
        }
    }
}
