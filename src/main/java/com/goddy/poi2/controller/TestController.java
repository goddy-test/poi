package com.goddy.poi2.controller;

import com.goddy.poi2.entity.Excel;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

@RestController
public class TestController {

    @RequestMapping("/test")
    public void test(HttpServletResponse response) throws Exception {
        //假数据
        Map<String, List<String>> mock = new HashMap<>();
        mock.put("id", asList("", ""));
        mock.put("name", asList("姚兴院", "肉丸子", "Goddy", "小刘", "刘一乐"));
        mock.put("sex", asList("male", "femal", "male", "male", "male"));
        mock.put("height", asList("177", "170", "180", "162", "171"));

        //创建工作薄
        Excel excel = new Excel("test", mock).withBuild();
        excel.export(response);
    }
}
